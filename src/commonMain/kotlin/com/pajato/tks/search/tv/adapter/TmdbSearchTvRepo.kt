package com.pajato.tks.search.tv.adapter

import com.pajato.tks.common.adapter.Strategy
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.SearchKey
import com.pajato.tks.pager.core.PageData
import com.pajato.tks.pager.core.PagedResultTv
import com.pajato.tks.search.tv.core.SearchTvRepo

/**
 * A singleton object that implements the [SearchTvRepo] interface, providing methods to search for TV shows using
 * The Movie Database (TMDb) API. This object manages a cache of search results to minimize network requests.
 */
public object TmdbSearchTvRepo : SearchTvRepo {
    internal val cache: MutableMap<SearchKey, PageData<PagedResultTv>> = mutableMapOf()

    /**
     * Retrieves the TV search results page based on the provided search key.
     * If the search result is cached, it returns the cached result, otherwise,
     * it fetches the results from the TMDb (The Movie Database) API.
     *
     * @param key The search key containing query and page number information.
     * @return The search page containing the list of TV search results.
     */
    override suspend fun getSearchPageTv(key: SearchKey): PageData<PagedResultTv> {
        val path = "search/tv"
        val extraParams = listOf("query=${key.query}", "page=${key.page}")
        val serializer: Strategy<PageData<PagedResultTv>> = PageData.serializer(PagedResultTv.serializer())
        val defaultPage = PageData<PagedResultTv>(0, listOf(), 0, 0)
        return cache[key] ?: TmdbFetcher.fetch(path, extraParams, key, serializer, defaultPage).also { cache[key] = it }
    }

    /**
     * Retrieves search results for TV shows based on the provided search key.
     *
     * @param key The search key that contains the query and page number information.
     * @return A list of TV search results.
     */
    override suspend fun getSearchResults(key: SearchKey): List<PagedResultTv> = getSearchPageTv(key).results
}
